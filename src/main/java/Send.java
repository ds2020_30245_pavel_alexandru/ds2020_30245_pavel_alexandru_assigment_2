import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import org.json.JSONObject;
import java.io.File;
import java.util.Scanner;
import java.util.UUID;

public class Send {

    private final static String QUEUE_NAME = "activity";

    public static void main(String[] argv) throws Exception {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("sparrow.rmq.cloudamqp.com");
        factory.setVirtualHost("qfzqeiob");
        factory.setUsername("qfzqeiob");
        factory.setPassword("3IuaIrSQKUYO_Lky5f8fNxohFnF_hQe5");

        try (Connection connection = factory.newConnection();

             Channel channel = connection.createChannel()) {

                    File file=new File("activity.txt");
                    Scanner scanner = new Scanner(file);

                    channel.queueDeclare(QUEUE_NAME, false, false, false, null);

                    while(scanner.hasNextLine())
                    {

                        String message = scanner.nextLine();
                        String[] fields= message.split("\t\t");

                        JSONObject jsonObject = new JSONObject();

                        String stringId = "47fe5a2a-90cb-4467-b40e-2bee69924f0e";
                        UUID id = UUID.fromString(stringId);
                        jsonObject.put("patient_id", id);

                        String activity = fields[2];
                        activity.replace("\t","");
                        jsonObject.put("activity", activity);

                        String start = fields[0];
                        jsonObject.put("start", start);

                        String end =fields[1];
                        jsonObject.put("end", end);

                        channel.basicPublish("", QUEUE_NAME, null, jsonObject.toString().getBytes());

                        System.out.println(" [x] Sent " + jsonObject.toString());

                        Thread.sleep(100);

                    }
        }
    }
}
